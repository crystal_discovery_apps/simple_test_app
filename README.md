# simple_test_app

Quick application to test Crystal langage

## Aims

Create a global application implementing simple an classical exemple of codes to discover a langage.

### Hello World as a package

The classic `Hello World` is quite short to make in crystal so lets package it and serve it :)

### Computation package

Implement some basic arithmetics functions:

- pow(a,b) = a^b
- a!
- Greater Common Divisor, Lower Common Multiple
- Solver for 2 degree equations

### Problem package

Implements some basic problem solver:

- Hanoi tower
- Einstein enigma

## Usage

Nothing mutch to be used for except a simple sample to reproduce

## Development

### Use as get started

- clone the project
- remove `computation`, `hello_world` and `problem` folders
- implement the aims

### Get started from scratch

- init an app using `crystal init app ${NAME}`
- implement the aims in `src`

## Run

- `crystal run src/${APP-NAME}.cr`

## Contributors

- [your-github-user](https://gitlab.com/titouanfreville) Titouan - creator, maintainer
