require "./simple_test_app/*"
require "./hello_world/*"
require "./computation/*"

# Module `SimpleTestApp` is used as a basic module to play with crystal for the first time.
# It implements lots of small funny things that will be called responding to users request.
# 
# NOTE: This is a note
module SimpleTestApp
  puts "Simple test application. Current version is: " + VERSION
  hello_world = HelloWorld.new
  print "Please enter your name: "
  user_name = gets
  hello_world.hello_world(user_name)
  interractive = Interractive.new(user_name)
  interractive.run
end
