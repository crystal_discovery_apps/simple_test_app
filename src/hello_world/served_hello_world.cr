require "http/server"
module SimpleTestApp
	class ServedHelloWorld
		def serve()
			server = HTTP::Server.new do |context|
				context.response.content_type = "text/plain"
				context.response.print "Hello world! The time is #{Time.now}"
				end

			server.bind_tcp 8080
			server.listen
		end
	end
    
end