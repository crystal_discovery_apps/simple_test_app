module SimpleTestApp
  class HelloWorld
    def hello_world(user_name)
      puts "Hello #{user_name}. Welcome in the simple test application for crystal starters."
      puts "Have fun discovering all the code here."
    end
  end
end