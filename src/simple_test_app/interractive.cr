require "../simple_test_app/*"
require "../hello_world/*"
require "../computation/*"

module SimpleTestApp
  class Interractive
    @hello_world : HelloWorld = HelloWorld.new
    @computation : SimpleComputation = SimpleComputation.new
    @hello_world_server : ServedHelloWorld = ServedHelloWorld.new

    def initialize(@name : String | Nil)
    end

    def say_hello()
      @hello_world.hello_world(@name)
    end
  
    def serve()
      @hello_world_server.serve
    end
  
    def calcul_pow()
      puts "Calculating a^b"
      print "a: "
      a = gets
      print "b: "
      b = gets
      res = 0
      if a && b
        res = @computation.pow(b.to_f,a.to_f)
      end
      puts "#{a}^#{b} = #{res}"
    end

    def calcul_fact()
      puts "Calculating a!"
      print "a: "
      a = gets
      res = 0
      if a
        res = @computation.fact(a.to_f)
      end
      puts "#{a}! = #{res}"
    end
  
    def calcul_gcd()
      puts "Calculating gcd(a,b)"
      print "a: "
      a = gets
      print "b: "
      b = gets
      res = 0
      if a && b
        res = @computation.gcd(a.to_f,b.to_f)
      end
      puts "gcd(#{a},#{b}) = #{res}"
    end
  
    def calcul_lcm()
      puts "Calculating lcm(a,b)"
      print "a: "
      a = gets
      print "b: "
      b = gets
      res = 0
      if a && b
        res = @computation.lcm(a.to_f,b.to_f)
      end
      puts "lcm(#{a},#{b}) = #{res}"
    end

    def solve_second_degree
      puts "Solving ax^2+bx+c"
      print "a: "
      a = gets
      print "b: "
      b = gets
      print "c: "
      c = gets
      res = {nil, nil, nil, nil}
      if a && b && c
        res = @computation.second_degree_solver(a.to_f, b.to_f, c.to_f)
      end
      if res[2] && res[1] && res[3]
        puts "x1: #{res[0]} + #{res[1]}i. x2: #{res[2]} + #{res[3]}i."
        return
      end
      if res[2] && !res[1] && !res[3]
        puts "x1: #{res[0]}. x2: #{res[2]}."
        return
      end
      if !res[2] && res[1] && !res[3]
        puts "x1: #{res[0]} + #{res[1]}i."
        return
      end
      puts "x1: #{res[0]}."
    end

    def print_menu()
      puts
      puts "--------------------------"
      puts "What do you wish to do ?"
      puts "1- Say Hello"
      puts "2- Run hello in a server (no escape)"
      puts "3- a^b"
      puts "4- a!"
      puts "5- gcd(a,b)"
      puts "6- lcm(a,b)"
      puts "7- solve: a*x^2 + b*x + c"
      puts "exit- Leave :'("
      puts "-------------------------"
    end
    
    def run()
      exit = false
      while !exit
        self.print_menu()
        choice = gets
        case choice
        when "exit", "leave", "out", "sortie", "fin", "end"
          exit=1
        when "1"
          self.say_hello
        when "2"
          self.serve
        when "3"
          self.calcul_pow
        when "4"
          self.calcul_fact
        when "5"
          self.calcul_gcd
        when "6"
          self.calcul_lcm
        when "7"
          self.solve_second_degree
        end
      end
    end
  end
end