module SimpleTestApp
  class SimpleComputation
    def pow(a,b)
      return a > 0 ?  b*pow((a-1), b) : 1
    end

    def fact(a)
      return a > 0 ? a*fact(a-1) : 1
    end

    def gcd(a,b)
      rest = a%b
      return rest > 0 ? gcd(b,rest) : b
    end

    def lcm(a,b)
      return a*b/gcd(a,b)
    end

    # second_degree_solver(a,b,c)
    #
    # Solve a x^2 + bx + c = 0 equation where a != 0
    #
    # @param a float
    # @param b float
    # @param c float
    # @return reel_res1, img_res1, reel_res2, img_res2
    # If result is reel, img_resX will be nil.
    # If equation as a unique solution, only reel_res1 will be define
    def second_degree_solver(a,b,c)
      delta = b*b - 4*a*c 
      if delta > 0
        return (-b+Math.sqrt(delta))/(2*a), nil, (-b-Math.sqrt(delta))/(2*a), nil
      elsif delta == 0 
        return -b/(2*a), nil, nil, nil
      else 
        return -b/(2*a), Math.sqrt(-delta)/(2*a), -b/(2*a), -Math.sqrt(-delta)/(2*a)
      end
    end
  end
end